package com.codechimp.beerDemoSpringBoot;

import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.InternalSettingsPreparer;
import org.elasticsearch.node.Node;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.transport.Netty3Plugin;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

/**
 * Base test class that starts an embedded Elasticsearch instance in order to allow the tests to run in a compact fashion
 */
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractElasticsearchTest {

    private static final String HTTP_PORT = "9205";
    private static final String HTTP_TRANSPORT_PORT = "9305";
    private static final String ES_WORKING_DIR = "target/es";

    private static Node node;

    @BeforeClass
    public static void startElasticsearch() throws Exception {
        removeOldDataDir(ES_WORKING_DIR);

        Settings settings = Settings.builder()
                .put("path.home", ES_WORKING_DIR)
                .put("path.conf", ES_WORKING_DIR)
                .put("path.data", ES_WORKING_DIR)
                .put("path.logs", ES_WORKING_DIR)
                .put("http.port", HTTP_PORT)
                .put("http.type","netty3")
                .put("transport.tcp.port", HTTP_TRANSPORT_PORT)
                .put("transport.type","local")
                .build();

        node = new PluginConfigurableNode(settings, Collections.singletonList(Netty3Plugin.class));
        node.start();
    }

    @AfterClass
    public static void stopElasticsearch() throws Exception {
        if (node != null) node.close();
    }

    private static void removeOldDataDir(String datadir) throws Exception {
        File dataDir = new File(datadir);
        if (dataDir.exists()) {
            FileSystemUtils.deleteRecursively(dataDir);
        }
    }

    /**
     * Extension of the {@link Node} class that allows setting plugins
     */
    private static class PluginConfigurableNode extends Node {
        PluginConfigurableNode(Settings settings, Collection<Class<? extends Plugin>> classpathPlugins) {
            super(InternalSettingsPreparer.prepareEnvironment(settings, null), classpathPlugins);
        }
    }
}