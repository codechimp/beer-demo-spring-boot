package com.codechimp.beerDemoSpringBoot.controller;

import com.codechimp.beerDemoSpringBoot.BeerDemoSpringBootApplication;
import com.codechimp.beerDemoSpringBoot.model.Brewery;
import com.codechimp.beerDemoSpringBoot.service.BreweryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.entity.ContentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test for the {@link BreweryApi}
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BeerDemoSpringBootApplication.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BreweryApiTest {
    @Autowired private MockMvc mockMvc;
    @Autowired private BreweryApi breweryApi;
    @Autowired private BreweryService brewerySvc;

    /**
     * Tests that the {@link BreweryApi} properly throws a {@link ResourceNotFoundException} when a bogus id is requested
     */
    @Test(expected = ResourceNotFoundException.class)
    public void testGetBreweryThrowsResourceNotFoundException() {
        breweryApi.getBrewery(99999L);
    }

    /**
     * Tests that calling the {@link BreweryApi} through the MVC framework returns a 404 "Not Found" for a bogus {@link Brewery} Id
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testGetBrewery404() throws Exception {
        mockMvc.perform(get("/api/v1/brewery/999999"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * Tests that a new {@link Brewery} can be created
     * @throws Exception Any exception that might be thrown
     */
    @Test
    public void testCreate() throws Exception {
        Brewery newBrewery = new Brewery(RandomStringUtils.randomAlphanumeric(3, 10), RandomStringUtils.randomAlphabetic(10,40));

        MvcResult result = mockMvc.perform(put("/api/v1/brewery")
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(newBrewery)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Brewery resultBrewery = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Brewery.class);
        assertBrewery(newBrewery, resultBrewery);
    }

    /**
     * Tests that a {@link Brewery}  can be altered and saved
     * @throws Exception Any exception that might be thrown
     */
    @Test
    public void testSave() throws Exception {
        Brewery newBrewery = new Brewery(RandomStringUtils.randomAlphanumeric(3, 10), RandomStringUtils.randomAlphabetic(10,40));

        MvcResult result = mockMvc.perform(put("/api/v1/brewery")
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(newBrewery)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Brewery resultBrewery = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Brewery.class);
        assertBrewery(newBrewery, resultBrewery);

        resultBrewery.setName("New name");
        resultBrewery.setDescription("New desc");

        MvcResult result2 = mockMvc.perform(post(String.format("/api/v1/brewery/%d", resultBrewery.getId()))
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(resultBrewery)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Brewery resultBrewery2 = new ObjectMapper().readValue(result2.getResponse().getContentAsString(), Brewery.class);
        assertBrewery(resultBrewery, resultBrewery2);
        Assert.assertEquals(resultBrewery.getId(), resultBrewery2.getId());
    }

    /**
     * Tests getting all {@link Brewery}s
     * @throws Exception Any exception that might be thrown
     */
    @Test
    public void testGetBreweries() throws Exception {
        List<Brewery> breweries = IntStream.range(0, 40)
                .mapToObj(i -> new Brewery(RandomStringUtils.randomAlphanumeric(3, 10), RandomStringUtils.randomAlphabetic(10,40)))
                .collect(Collectors.toList());
        List<Brewery> results = breweries.stream().map(brewerySvc::saveBrewery).collect(Collectors.toList());

        mockMvc.perform(get("/api/v1/brewery"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(20)))  // This is the default page size if one is not provided
                .andExpect(jsonPath("$.totalElements", is(results.size())));
    }

    /**
     * Tests getting all {@link Brewery}s with custom page and size
     * @throws Exception Any exception that might occur
     */
    @Test
    public void testGetBreweriesPageable() throws Exception {
        final int PAGE_SIZE = 10;
        List<Brewery> breweries = IntStream.range(0, 40)
                .mapToObj(i -> new Brewery(RandomStringUtils.randomAlphanumeric(3, 10), RandomStringUtils.randomAlphabetic(10,40)))
                .collect(Collectors.toList());
        List<Brewery> results = breweries.stream().map(brewerySvc::saveBrewery).collect(Collectors.toList());

        mockMvc.perform(get(String.format("/api/v1/brewery?page=0&size=%d", PAGE_SIZE)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number", is(0)))
                .andExpect(jsonPath("$.content", hasSize(PAGE_SIZE)))
                .andExpect(jsonPath("$.first", is(true)))
                .andExpect(jsonPath("$.last", is(false)))
                .andExpect(jsonPath("$.size", is(PAGE_SIZE)))
                .andExpect(jsonPath("$.totalPages", is(results.size() / PAGE_SIZE)))
                .andExpect(jsonPath("$.totalElements", is(results.size())));
    }

    /**
     * Tests that a {@link Brewery} can be properly removed
     * @throws Exception Any exception that might occur
     */
    @Test
    public void testRemove() throws Exception {
        Brewery newBrewery = new Brewery(RandomStringUtils.randomAlphanumeric(3, 10), RandomStringUtils.randomAlphabetic(10,40));
        Brewery result = brewerySvc.saveBrewery(newBrewery);

        mockMvc.perform(get(String.format("/api/v1/brewery/%d", result.getId())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(new ObjectMapper().writeValueAsString(result))));

        mockMvc.perform(delete(String.format("/api/v1/brewery/%d", result.getId())))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get(String.format("/api/v1/brewery/%d", result.getId())))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * Tests the original {@link Brewery} against the resulting <code>Brewery</code>
     * @param orig The original <code>Brewery</code>
     * @param result The resulting <code>Brewery</code> to compare to.
     */
    private void assertBrewery(Brewery orig, Brewery result) {
        Assert.assertNotNull(orig);
        Assert.assertNotNull(result);
        Assert.assertNotEquals(0, result.getId());
        Assert.assertEquals(orig.getName(), result.getName());
        Assert.assertEquals(orig.getDescription(), result.getDescription());
    }
}
