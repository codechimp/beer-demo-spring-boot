package com.codechimp.beerDemoSpringBoot.controller;

import com.codechimp.beerDemoSpringBoot.BeerDemoSpringBootApplication;
import com.codechimp.beerDemoSpringBoot.model.BeerStyle;
import com.codechimp.beerDemoSpringBoot.service.BeerStyleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.entity.ContentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test for the {@link BeerStyleApi}
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BeerDemoSpringBootApplication.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BeerStyleApiTest {

    @Autowired private MockMvc mockMvc;
    @Autowired BeerStyleApi styleApi;
    @Autowired BeerStyleService styleSvc;

    /**
     * Tests that the {@link BeerStyleApi} properly throws a {@link ResourceNotFoundException} when requesting a bogus beer by ID
     */
    @Test(expected = ResourceNotFoundException.class)
    public void testGetBeerStyleThrowsResourceNotFoundException() {
        styleApi.getBeerStyle(999999L);
    }

    /**
     * Tests that calling the {@link BeerStyleApi} through the MVC framework returns a 404 "Not Found" for a bogus {@link BeerStyle} Id
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testGetBeerStyle404() throws Exception {
        mockMvc.perform(get("/api/v1/beer/999999"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * Tests creating a new {@link BeerStyle} through the API
     * @throws Exception Any exception that might occur
     */
    @Test
    public void testCreate() throws Exception {
        BeerStyle newStyle = new BeerStyle(RandomStringUtils.randomAlphabetic(3, 10), RandomStringUtils.randomAlphanumeric(10, 25));

        MvcResult result = mockMvc.perform(put("/api/v1/beerstyle")
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(newStyle)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        BeerStyle respStyle = new ObjectMapper().readValue(result.getResponse().getContentAsString(), BeerStyle.class);
        assertStyle(newStyle, respStyle);
    }

    /**
     * Tests creating a new {@link BeerStyle} through the API
     * @throws Exception Any exception that might occur
     */
    @Test
    public void testSave() throws Exception {
        BeerStyle newStyle = new BeerStyle(RandomStringUtils.randomAlphabetic(3, 10), RandomStringUtils.randomAlphanumeric(10, 25));

        MvcResult result = mockMvc.perform(put("/api/v1/beerstyle")
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(newStyle)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        BeerStyle respStyle = new ObjectMapper().readValue(result.getResponse().getContentAsString(), BeerStyle.class);
        assertStyle(newStyle, respStyle);

        respStyle.setName("New Name");
        respStyle.setDescription("New Desc");

        MvcResult result2 = mockMvc.perform(post(String.format("/api/v1/beerstyle/%d", respStyle.getId()))
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(respStyle)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        BeerStyle respStyle2 = new ObjectMapper().readValue(result2.getResponse().getContentAsString(), BeerStyle.class);
        assertStyle(respStyle, respStyle2);
        Assert.assertEquals(respStyle.getId(), respStyle2.getId());
    }

    /**
     * Tests getting all {@link BeerStyle}s
     * @throws Exception Any exception that might occur
     */
    @Test
    public void testGetBeerStyles() throws Exception {
       List<BeerStyle> styles = IntStream.range(0, 40)
               .mapToObj(i -> new BeerStyle(RandomStringUtils.randomAlphanumeric(3, 10), RandomStringUtils.randomAlphabetic(10, 30)))
               .collect(Collectors.toList());
       List<BeerStyle> results = styles.stream().map(styleSvc::saveBeerStyle).collect(Collectors.toList());

        mockMvc.perform(get("/api/v1/beerstyle"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(20)))  // This is the default page size if one is not provided
                .andExpect(jsonPath("$.totalElements", is(results.size())));
    }

    /**
     * Tests retrieving all {@link BeerStyle}s in a {@link sun.jvm.hotspot.debugger.Page}
     * @throws Exception
     */
    @Test
    public void testGetBeerStylesPageable() throws Exception {
        final int PAGE_SIZE = 10;
        List<BeerStyle> styles = IntStream.range(0, 40)
                .mapToObj(i -> new BeerStyle(RandomStringUtils.randomAlphanumeric(3, 10), RandomStringUtils.randomAlphabetic(10, 30)))
                .collect(Collectors.toList());
        List<BeerStyle> results = styles.stream().map(s -> styleSvc.saveBeerStyle(s)).collect(Collectors.toList());

        mockMvc.perform(get(String.format("/api/v1/beerstyle?page=0&size=%d", PAGE_SIZE)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number", is(0)))
                .andExpect(jsonPath("$.content", hasSize(PAGE_SIZE)))
                .andExpect(jsonPath("$.first", is(true)))
                .andExpect(jsonPath("$.last", is(false)))
                .andExpect(jsonPath("$.size", is(PAGE_SIZE)))
                .andExpect(jsonPath("$.totalPages", is(results.size() / PAGE_SIZE)))
                .andExpect(jsonPath("$.totalElements", is(results.size())));
    }

    /**
     * Tests that we can properly remove a {@link BeerStyle}
     * @throws Exception Any exception that might occur
     */
    @Test
    public void testRemove() throws Exception {
        BeerStyle style = new BeerStyle(RandomStringUtils.randomAlphanumeric(3, 10), RandomStringUtils.randomAlphabetic(10, 30));
        BeerStyle result = styleSvc.saveBeerStyle(style);

        mockMvc.perform(get(String.format("/api/v1/beerstyle/%d", result.getId())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(new ObjectMapper().writeValueAsString(result))));

        mockMvc.perform(delete(String.format("/api/v1/beerstyle/%d", result.getId())))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get(String.format("/api/v1/beerstyle/%d", result.getId())))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * Utility function to check the "original" with the "result" style
     * @param orig The original {@link BeerStyle}
     * @param result The resulting <code>BeerStyle</code>
     */
    private void assertStyle(BeerStyle orig, BeerStyle result) {
        Assert.assertNotNull(orig);
        Assert.assertNotNull(result);
        Assert.assertNotEquals(0, result.getId());
        Assert.assertEquals(orig.getName(), result.getName());
        Assert.assertEquals(orig.getDescription(), result.getDescription());
    }
}
