package com.codechimp.beerDemoSpringBoot.controller;

import com.codechimp.beerDemoSpringBoot.BeerDemoSpringBootApplication;
import com.codechimp.beerDemoSpringBoot.model.Beer;
import com.codechimp.beerDemoSpringBoot.model.BeerDocument;
import com.codechimp.beerDemoSpringBoot.service.BeerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.http.entity.ContentType;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Unit tests for the {@link BeerApi}
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BeerDemoSpringBootApplication.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BeerApiTest {

    @Autowired private ElasticsearchTemplate esTemplate;

    @Autowired private MockMvc mockMvc;
    @Autowired private BeerApi beerApi;
    @Autowired private BeerService beerSvc;

    @Before @After
    public void clearData() {
        // Wipe the Elasticsearch data (it's an external instance, so we need a "clean slate")
        esTemplate.deleteIndex(BeerDocument.class);
        esTemplate.createIndex(BeerDocument.class);
        esTemplate.putMapping(BeerDocument.class);
        esTemplate.refresh(BeerDocument.class);

        // Remove all beers from the DB
        beerSvc.getBeers().forEach(beer -> beerSvc.removeBeer(beer.getId()));
    }

    /**
     * Tests that the {@link BeerApi} properly throws a {@link ResourceNotFoundException} when requesting a bogus beer by ID
     */
    @Test(expected = ResourceNotFoundException.class)
    public void testGetBeerThrowsResourceNotFoundException() {
        beerApi.getBeer(999999L);
    }

    /**
     * Tests that calling the {@link BeerApi} through the MVC framework returns a 404 "Not Found" for a bogus {@link Beer} Id
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testGetBeer404() throws Exception {
        mockMvc.perform(get("/api/v1/beer/999999"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * Tests saving a new Beer
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testCreateBeer() throws Exception {
        Beer newBeer = new Beer();
        newBeer.setName("Test Beer");
        newBeer.setDescription("Test Beer description");
        newBeer.setIbu(1.2);
        newBeer.setAbv(2.2);

        MvcResult result = mockMvc.perform(put("/api/v1/beer")
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(newBeer)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Beer respBeer = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Beer.class);
        Assert.assertNotNull(respBeer);
        Assert.assertNotEquals(0, respBeer.getId());
        Assert.assertNotNull(respBeer.getName());
        Assert.assertEquals(newBeer.getName(), respBeer.getName());
        Assert.assertNotNull(respBeer.getDescription());
        Assert.assertEquals(newBeer.getDescription(), respBeer.getDescription());
        Assert.assertEquals(newBeer.getAbv(), respBeer.getAbv(), 0.0);
        Assert.assertEquals(newBeer.getIbu(), respBeer.getIbu(), 0.0);
    }

    /**
     * Tests saving a new Beer
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testSaveBeer() throws Exception {
        Beer newBeer = new Beer();
        newBeer.setName("Test Beer Before");
        newBeer.setDescription("Test Beer Before description");
        newBeer.setIbu(2.0);
        newBeer.setAbv(2.0);

        MvcResult result = mockMvc.perform(put("/api/v1/beer")
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(newBeer)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Beer respBeer = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Beer.class);
        Assert.assertNotNull(respBeer);
        Assert.assertNotEquals(0, respBeer.getId());
        Assert.assertNotNull(respBeer.getName());
        Assert.assertEquals(newBeer.getName(), respBeer.getName());
        Assert.assertNotNull(respBeer.getDescription());
        Assert.assertEquals(newBeer.getDescription(), respBeer.getDescription());
        Assert.assertEquals(newBeer.getAbv(), respBeer.getAbv(), 0.0);
        Assert.assertEquals(newBeer.getIbu(), respBeer.getIbu(), 0.0);

        respBeer.setName("Test Beer After");
        respBeer.setDescription("Test Beer After description");
        respBeer.setAbv(3.0);
        respBeer.setIbu(4.0);

        MvcResult result2 = mockMvc.perform(post(String.format("/api/v1/beer/%d", respBeer.getId()))
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .content(new ObjectMapper().writeValueAsString(respBeer)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Beer respBeer2 = new ObjectMapper().readValue(result2.getResponse().getContentAsString(), Beer.class);
        Assert.assertNotNull(respBeer2);
        Assert.assertNotEquals(0, respBeer2.getId());
        Assert.assertNotNull(respBeer2.getName());
        Assert.assertEquals(respBeer.getName(), respBeer2.getName());
        Assert.assertNotEquals(newBeer.getName(), respBeer2.getName());
        Assert.assertNotNull(respBeer2.getDescription());
        Assert.assertEquals(respBeer.getDescription(), respBeer2.getDescription());
        Assert.assertNotEquals(newBeer.getDescription(), respBeer2.getDescription());
        Assert.assertEquals(respBeer.getAbv(), respBeer2.getAbv(), 0.0);
        Assert.assertNotEquals(newBeer.getAbv(), respBeer2.getAbv(), 0.0);
        Assert.assertEquals(respBeer.getIbu(), respBeer2.getIbu(), 0.0);
        Assert.assertNotEquals(newBeer.getIbu(), respBeer2.getIbu(), 0.0);
    }

    /**
     * Tests retrieving a beer by ID
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testGetBeer() throws Exception {
        Beer newBeer = new Beer();
        newBeer.setName("New Beer");
        newBeer.setDescription("New Beer description");
        newBeer.setIbu(1.0);
        newBeer.setAbv(2.0);
        Beer respBeer = beerSvc.saveBeer(newBeer);

        mockMvc.perform(get(String.format("/api/v1/beer/%d", respBeer.getId())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(new ObjectMapper().writeValueAsString(respBeer))));
    }

    /**
     * Tests that the getBeers response is correct
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testGetBeers() throws Exception {
        List<Beer> beers = IntStream.range(0, 5)
                .mapToObj(i -> buildRandomBeer())
                .collect(Collectors.toList());

        List<Beer> resultBeers = beers.stream().map(beer -> beerSvc.saveBeer(beer)).collect(Collectors.toList());

        mockMvc.perform(get("/api/v1/beer"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(resultBeers.size())))
                .andExpect(jsonPath("$.totalElements", is(resultBeers.size())));
    }

    /**
     * Tests that the getBeers response is correct
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testGetBeersPageable() throws Exception {
        final int PAGE_SIZE = 10;
        List<Beer> beers = IntStream.range(0, 40)
                .mapToObj(i -> buildRandomBeer())
                .collect(Collectors.toList());

        List<Beer> resultBeers = beers.stream().map(beer -> beerSvc.saveBeer(beer)).collect(Collectors.toList());

        mockMvc.perform(get("/api/v1/beer?page=0&size=10"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number", is(0)))
                .andExpect(jsonPath("$.content", hasSize(PAGE_SIZE)))
                .andExpect(jsonPath("$.first", is(true)))
                .andExpect(jsonPath("$.last", is(false)))
                .andExpect(jsonPath("$.size", is(PAGE_SIZE)))
                .andExpect(jsonPath("$.totalPages", is(resultBeers.size() / PAGE_SIZE)))
                .andExpect(jsonPath("$.totalElements", is(resultBeers.size())));
    }

    /**
     * Tests the removal of a {@link Beer} through the API
     * @throws Exception Any exception that occurs during the test
     */
    @Test
    public void testRemoveBeer() throws Exception {
        Beer newBeer = new Beer();
        newBeer.setName("Beer");
        newBeer.setDescription("Beer description");
        newBeer.setIbu(3.0);
        newBeer.setAbv(2.2);
        Beer respBeer = beerSvc.saveBeer(newBeer);

        mockMvc.perform(get(String.format("/api/v1/beer/%d", respBeer.getId())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(new ObjectMapper().writeValueAsString(respBeer))));

        mockMvc.perform(delete(String.format("/api/v1/beer/%d", respBeer.getId())))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get(String.format("/api/v1/beer/%d", respBeer.getId())))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * Builds a randomized {@link Beer}
     * @return The randomized <code>Beer</code>
     */
    private Beer buildRandomBeer() {
        Beer beer = new Beer();
        beer.setName(RandomStringUtils.randomAlphanumeric(3, 10));
        beer.setDescription(RandomStringUtils.randomAlphabetic(5, 25));
        beer.setIbu(RandomUtils.nextDouble());
        beer.setAbv(RandomUtils.nextDouble());
        return beer;
    }
}
