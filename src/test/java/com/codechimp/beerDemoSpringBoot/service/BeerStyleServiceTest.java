package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.BeerDemoSpringBootApplication;
import com.codechimp.beerDemoSpringBoot.model.BeerStyle;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;

/**
 * Unit test for the {@link BeerStyleService}
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BeerDemoSpringBootApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BeerStyleServiceTest {
    @Autowired private BeerStyleService styleSvc;

    @Before
    public void before() { }

    @Test
    public void testSave() {
        BeerStyle style = new BeerStyle();
        style.setName("Test Style");
        style.setDescription("Test Desc");
        BeerStyle resultStyle = styleSvc.saveBeerStyle(style);
        assertStyleResults(style, resultStyle);
    }

    @Test
    public void testGetBeerStyles() throws Exception {
        List<BeerStyle> origStyles = new ArrayList<>();
        List<BeerStyle> resultStyles = new ArrayList<>();
        for (int i=0; i<RandomUtils.nextInt(5, 20); i++) {
            BeerStyle style = new BeerStyle();
            style.setName(RandomStringUtils.randomAlphanumeric(5, 25));
            style.setDescription(RandomStringUtils.randomAlphabetic(10, 50));
            resultStyles.add(styleSvc.saveBeerStyle(style));
        }

        List<BeerStyle> retrievedStyles = styleSvc.getBeerStyles();
        Assert.assertEquals(resultStyles.size(), retrievedStyles.size());
        retrievedStyles.forEach(style -> Assert.assertThat(resultStyles, hasItem(style)));
    }

    @Test
    public void testGetBeerStylesPageable() throws Exception {
        List<BeerStyle> origStyles = new ArrayList<>();
        List<BeerStyle> resultStyles = new ArrayList<>();
        for (int i=0; i<RandomUtils.nextInt(25, 30); i++) {
            BeerStyle style = new BeerStyle();
            style.setName(RandomStringUtils.randomAlphanumeric(5, 25));
            style.setDescription(RandomStringUtils.randomAlphabetic(10, 50));
            resultStyles.add(styleSvc.saveBeerStyle(style));
        }

        Page<BeerStyle> page = styleSvc.getBeerStyles(PageRequest.of(0, 10));
        Assert.assertEquals(resultStyles.size(), page.getTotalElements());
        Assert.assertEquals((resultStyles.size() + 9) / 10, page.getTotalPages());
        Assert.assertEquals(10, page.getContent().size());
    }

    @Test
    public void testRemoveBeerStyle() throws Exception {
        List<BeerStyle> origStyles = new ArrayList<>();
        List<BeerStyle> resultStyles = new ArrayList<>();
        for (int i=0; i<RandomUtils.nextInt(5, 10); i++) {
            BeerStyle style = new BeerStyle();
            style.setName(RandomStringUtils.randomAlphanumeric(5, 25));
            style.setDescription(RandomStringUtils.randomAlphabetic(10, 50));
            resultStyles.add(styleSvc.saveBeerStyle(style));
        }

        List<BeerStyle> retrievedStyles = styleSvc.getBeerStyles();
        styleSvc.removeBeerStyle(resultStyles.get(0).getId());
        List<BeerStyle> retrievedStylesAfter = styleSvc.getBeerStyles();

        Assert.assertEquals(retrievedStyles.size() - 1, retrievedStylesAfter.size());
        Assert.assertThat(retrievedStylesAfter, not(hasItem(retrievedStyles.get(0))));
    }

    private void assertStyleResults(BeerStyle orig, BeerStyle result) {
        Assert.assertNotNull("Original style should not be null", orig);
        Assert.assertNotNull("Resulting style should not be null", result);
        Assert.assertNotEquals("Resulting style should have a generated non-zero id", 0, result.getId());
        Assert.assertEquals("Resulting style should have the same name", orig.getName(), result.getName());
        Assert.assertEquals("Resulting style should have the same description", orig.getDescription(), result.getDescription());
    }
}
