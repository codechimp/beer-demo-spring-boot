package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.BeerDemoSpringBootApplication;
import com.codechimp.beerDemoSpringBoot.model.Brewery;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;

/**
 * Unit test for the BreweryService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BeerDemoSpringBootApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BreweryServiceTest {

    @Autowired private BreweryService brewerySvc;

    @Before
    public void before() { }

    @Test
    public void testSave() throws Exception {
        Brewery brewery = new Brewery();
        brewery.setName(RandomStringUtils.randomAlphanumeric(5,20));
        brewery.setDescription(RandomStringUtils.randomAlphabetic(10,30));
        Brewery result = brewerySvc.saveBrewery(brewery);
        assertBrewery(brewery, result);
    }

    @Test
    public void testGetBreweries() throws Exception {
        List<Brewery> origs = new ArrayList<>();
        List<Brewery> results = new ArrayList<>();
        for(int i=0; i<RandomUtils.nextInt(5, 20); i++) {
            Brewery brewery = new Brewery();
            brewery.setName(RandomStringUtils.randomAlphanumeric(5,20));
            brewery.setDescription(RandomStringUtils.randomAlphabetic(10,30));
            origs.add(brewery);
            results.add(brewerySvc.saveBrewery(brewery));
        }

        List<Brewery> retrieved = brewerySvc.getBreweries();
        Assert.assertEquals(results.size(), retrieved.size());
        retrieved.forEach(brewery -> Assert.assertThat(results, hasItem(brewery)));
    }

    @Test
    public void testGetBreweriesPageable() throws Exception {
        List<Brewery> origs = new ArrayList<>();
        List<Brewery> results = new ArrayList<>();
        for(int i=0; i<RandomUtils.nextInt(25, 30); i++) {
            Brewery brewery = new Brewery();
            brewery.setName(RandomStringUtils.randomAlphanumeric(5,20));
            brewery.setDescription(RandomStringUtils.randomAlphabetic(10,30));
            origs.add(brewery);
            results.add(brewerySvc.saveBrewery(brewery));
        }

        Page<Brewery> page = brewerySvc.getBreweries(PageRequest.of(0,10));
        Assert.assertEquals(results.size(), page.getTotalElements());
        Assert.assertEquals((results.size() + 9) / 10, page.getTotalPages());
        Assert.assertEquals(10, page.getContent().size());
    }

    @Test
    public void testRemove() {
        List<Brewery> origs = new ArrayList<>();
        List<Brewery> results = new ArrayList<>();
        for(int i=0; i<RandomUtils.nextInt(25, 30); i++) {
            Brewery brewery = new Brewery();
            brewery.setName(RandomStringUtils.randomAlphanumeric(5,20));
            brewery.setDescription(RandomStringUtils.randomAlphabetic(10,30));
            origs.add(brewery);
            results.add(brewerySvc.saveBrewery(brewery));
        }

        List<Brewery> retrieved = brewerySvc.getBreweries();
        brewerySvc.removeBrewery(retrieved.get(0).getId());
        List<Brewery> retrievedAfter = brewerySvc.getBreweries();

        Assert.assertEquals(retrieved.size() - 1, retrievedAfter.size());
        Assert.assertThat(retrievedAfter, not(hasItem(retrieved.get(0))));
    }

    private void assertBrewery(Brewery orig, Brewery result) {
        Assert.assertNotEquals(0, result.getId());
        Assert.assertEquals(orig.getName(), result.getName());
        Assert.assertEquals(orig.getDescription(), result.getDescription());
    }
}
