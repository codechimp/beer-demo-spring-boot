package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.BeerDemoSpringBootApplication;
import com.codechimp.beerDemoSpringBoot.model.Beer;
import com.codechimp.beerDemoSpringBoot.model.BeerCompletionResult;
import com.codechimp.beerDemoSpringBoot.model.BeerDocument;
import com.codechimp.beerDemoSpringBoot.model.BeerStyle;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.IntStream;

/**
 * Unit tests for the {@link BeerService}
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BeerDemoSpringBootApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BeerServiceTest {

    @Autowired private BeerService beerService;
    @Autowired private BeerStyleService styleService;

    @Autowired private ElasticsearchTemplate esTemplate;

    private Beer beer1;
    private Beer result1;

    private Beer beer2;
    private Beer result2;

    private Beer beer3;
    private Beer result3;

    /**
     * Cleans up the ES index and recreates a standard set of {@link Beer}s
     */
    @Before
    public void before() {
        // Wipe the Elasticsearch data (it's an external instance, so we need a "clean slate")
        esTemplate.deleteIndex(BeerDocument.class);
        esTemplate.createIndex(BeerDocument.class);
        esTemplate.putMapping(BeerDocument.class);
        esTemplate.refresh(BeerDocument.class);

        beer1 = new Beer();
        beer1.setName("TestNoSpace");
        beer1.setDescription("Test Description");
        beer1.setAbv(1.1);
        beer1.setIbu(2.2);
        result1 = beerService.saveBeer(beer1);

        beer2 = new Beer();
        beer2.setName("Test Space");
        beer2.setDescription("Some other description");
        beer2.setAbv(3.1);
        beer2.setIbu(0.1);
        result2 = beerService.saveBeer(beer2);

        beer3 = new Beer();
        beer3.setName("Other Beer");
        beer3.setDescription("Test Description");
        beer3.setAbv(5);
        beer3.setIbu(1);
        result3 = beerService.saveBeer(beer3);
    }

    /**
     * Cleans up the ES index
     */
    @After
    public void after() {
        // Wipe the Elasticsearch data (it's an external instance, so we need a "clean slate")
        esTemplate.deleteIndex(BeerDocument.class);
        esTemplate.createIndex(BeerDocument.class);
        esTemplate.putMapping(BeerDocument.class);
        esTemplate.refresh(BeerDocument.class);
    }

    /**
     * Tests saving a simple {@link Beer} and then retrieving it from Elasticsearch
     */
    @Test
    public void testSimpleSaveAndRetrieveFromES() {
        Page<BeerDocument> beerDocResults = beerService.searchBeersByName(beer1.getName());

        // Test that the Beer was properly saved
        assertBeerResults(beer1, result1);

        // Test that the Elasticsearch results were returned
        Assert.assertNotNull("The BeerDocument search results should not be null", beerDocResults);
        Assert.assertEquals("The BeerDocument search results should have 1 result", 1, beerDocResults.getContent().size());

        // Check that the resulting BeerDocument matches what we expect
        BeerDocument beerDoc = beerDocResults.iterator().next();
        assertBeerDocumentResults(result1, beerDoc);
        Assert.assertNull("The BeerDocument style id should be null", beerDoc.getStyleId());
        Assert.assertNull("The BeerDocument style name should be null", beerDoc.getStyleName());
        Assert.assertNull("The BeerDocument style description should be null", beerDoc.getStyleDescription());
        Assert.assertNull("The BeerDocument brewery id should be null", beerDoc.getBreweryId());
        Assert.assertNull("The BeerDocument brewery name should be null", beerDoc.getBreweryName());
        Assert.assertNull("The BeerDocument brewery description should be null", beerDoc.getBreweryDescription());
    }

    /**
     * Tests saving a {@link Beer} with a linked {@link BeerStyle}
     */
    @Test
    public void testSaveWithStyle() {
        BeerStyle style = new BeerStyle();
        style.setName("Style");
        style.setDescription("Style Description");
        style = styleService.saveBeerStyle(style);

        Beer beer = new Beer();
        beer.setName("Style Beer");
        beer.setDescription("Test Description");
        beer.setAbv(5);
        beer.setIbu(1);

        Beer result = beerService.saveBeer(beer, style.getId(), null);
        Assert.assertNotNull("The Beer should have the BeerStyle set", result.getStyle());
    }

    /**
     * Tests saving a {@link Beer} where the name has spaces, then retrieving that <code>Beer</code> from ES
     */
    @Test
    public void testBeerNameWithSpacesSaveAndRetrieveFromES() {
        Page<BeerDocument> beerDocResults = beerService.searchBeersByName(beer2.getName());

        // Test that the Beer was properly saved
        assertBeerResults(beer2, result2);

        // Test that the Elasticsearch results were returned
        Assert.assertNotNull("The BeerDocument search results should not be null", beerDocResults);
        Assert.assertEquals("The BeerDocument search results should have 2 result", 2, beerDocResults.getContent().size());

        // Check that the resulting BeerDocument matches what we expect
        BeerDocument beerDoc = beerDocResults.iterator().next();
        assertBeerDocumentResults(result2, beerDoc);
        Assert.assertNull("The BeerDocument style id should be null", beerDoc.getStyleId());
        Assert.assertNull("The BeerDocument style name should be null", beerDoc.getStyleName());
        Assert.assertNull("The BeerDocument style description should be null", beerDoc.getStyleDescription());
        Assert.assertNull("The BeerDocument brewery id should be null", beerDoc.getBreweryId());
        Assert.assertNull("The BeerDocument brewery name should be null", beerDoc.getBreweryName());
        Assert.assertNull("The BeerDocument brewery description should be null", beerDoc.getBreweryDescription());
    }

    /**
     * Test retrieving {@link Beer}s from ES using a partial name search
     */
    @Test
    public void testBeerNameSaveAndPartialRetrieveFromES() {
        Page<BeerDocument> beerDocResults = beerService.searchBeersByName("Test");

        // Test that the Beer was properly saved
        assertBeerResults(beer2, result2);

        // Test that the Elasticsearch results were returned
        Assert.assertNotNull("The BeerDocument search results should not be null", beerDocResults);
        Assert.assertEquals("The BeerDocument search results should have 2 result", 2, beerDocResults.getContent().size());

        // Check that the resulting BeerDocument matches what we expect
        assertBeerDocumentResults(beerDocResults);
    }

    /**
     * Tests retrieving {@link Beer}s from ES where there are two partial tokens
     */
    @Test
    public void testBeerNamePartialRetrieveFromESMultiple() {
        Page<BeerDocument> beerDocResults = beerService.searchBeersByName("Test Other");

        // Test that the Elasticsearch results were returned
        Assert.assertNotNull("The BeerDocument search results should not be null", beerDocResults);
        Assert.assertEquals("The BeerDocument search results should have 3 result", 3, beerDocResults.getContent().size());

        // Check that the resulting BeerDocument matches what we expect
        assertBeerDocumentResults(beerDocResults);
    }

    /**
     * Tests type-ahead search in ES
     */
    @Test
    public void testTypeAheadSearch() {
        List<BeerCompletionResult> beerDocResults = beerService.typeAheadSearchBeers("Tes");
        Assert.assertEquals("Should get back 2 results on type-ahead search", 2, beerDocResults.size());
    }

    /**
     * Asserts that the <code>orig</code> Beer matches the <code>result</code> Beer
     * @param orig The origin beer (prior to save)
     * @param results The result after JPA save
     */
    private void assertBeerResults(Beer orig, Beer results) {
        Assert.assertEquals("The new Beer should be .equals() to the resulting saved Beer", orig, results);
        Assert.assertNotEquals("The result should have a generated non-zero ID", 0,results.getId());
        Assert.assertEquals("The result should have the same name", orig.getName(), results.getName());
        Assert.assertEquals("The result should have the same description", orig.getDescription(), results.getDescription());
        Assert.assertEquals("The result should have the same ABV", orig.getAbv(), results.getAbv(), 0.0);
        Assert.assertEquals("The result should have the same IBU", orig.getIbu(), results.getIbu(), 0.0);
        Assert.assertNull("The result should have a null Brewery", results.getBrewery());
        Assert.assertNull("The result should have a null BeerStyle", results.getStyle());
    }

    /**
     * Compares a {@link BeerDocument} to the contents of a {@link Beer}
     * @param orig The original Beer (the one we need to compare against
     * @param results The BeerDocument to compare
     */
    private void assertBeerDocumentResults(Beer orig, BeerDocument results) {
        Assert.assertEquals("The BeerDocument ID should match our result's ID", orig.getId(),  results.getId());
        Assert.assertEquals("The BeerDocument name should match our result's name", orig.getName(), results.getName());
        Assert.assertEquals("The BeerDocument description should match our result's description", orig.getDescription(), results.getDescription());
        Assert.assertEquals("The BeerDocument ABV should match our result's ABV", orig.getAbv(), results.getAbv(), 0.0);
        Assert.assertEquals("The BeerDocument IBU should match our result's IBU", orig.getIbu(), results.getIbu(), 0.0);
    }

    /**
     * Compares each of the results from ES against the corresponding JPA result
     * @param beerDocResults The BeerDocument page results
     */
    private void assertBeerDocumentResults(Page<BeerDocument> beerDocResults) {
        beerDocResults.forEach(beerDoc -> {
            // Determine which results match
            Beer resultBeer = null;
            if (beerDoc.getId() == result1.getId()) {
                resultBeer = result1;
            } else if (beerDoc.getId() == result2.getId()) {
                resultBeer = result2;
            } else if (beerDoc.getId() == result3.getId()) {
                resultBeer = result3;
            }

            assertBeerDocumentResults(resultBeer, beerDoc);
        });
        final List<BeerDocument> content = beerDocResults.getContent();
        Assert.assertTrue(
                "Results should be sorted by score descending",
                IntStream.range(0, content.size() -1)
                        .allMatch(i -> Float.compare(content.get(i).getScore(), content.get(i+1).getScore()) >= 0));
    }
}
