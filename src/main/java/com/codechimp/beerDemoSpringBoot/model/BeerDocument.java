package com.codechimp.beerDemoSpringBoot.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;
import org.springframework.data.elasticsearch.core.completion.Completion;

/**
 * {@link Document} used to store/retrieve Beer info into Elasticsearch
 */
@Document(indexName = "beer", type = "beer")
@NoArgsConstructor @AllArgsConstructor
@ToString
public class BeerDocument implements Scoreable {
    @Id
    @Getter @Setter
    @Field(type = FieldType.Long, store = true)
    private long id;

    @Getter @Setter
    @Field(type = FieldType.Text, store = true)
    private String name;

    @Getter @Setter
    @CompletionField(maxInputLength = 100)
    private Completion name_completion;

    @Getter @Setter
    @Field(type = FieldType.Text)
    private String description;

    @Getter @Setter
    @CompletionField(maxInputLength = 100)
    private Completion description_completion;

    @Getter @Setter
    @Field(type = FieldType.Double)
    private double abv;

    @Getter @Setter
    @Field(type = FieldType.Double)
    private double ibu;

    @Getter @Setter
    @Field(type = FieldType.Long, store = true)
    private Long styleId;

    @Getter @Setter
    @Field(type = FieldType.Text)
    private String styleName;

    @Getter @Setter
    @Field(type = FieldType.Text)
    private String styleDescription;

    @Getter @Setter
    @Field(type = FieldType.Long, store = true)
    private Long breweryId;

    @Getter @Setter
    @Field(type = FieldType.Text)
    private String breweryName;

    @Getter @Setter
    @Field(type = FieldType.Text)
    private String breweryDescription;

    @Getter @Setter
    private float score;
}
