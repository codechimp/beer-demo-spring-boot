package com.codechimp.beerDemoSpringBoot.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Model class used to hold Elasticsearch completion/suggestion results
 */
@NoArgsConstructor @AllArgsConstructor
public class BeerCompletionResult implements Scoreable, Comparable<BeerCompletionResult> {
    @Getter @Setter
    private long id;

    @Getter @Setter
    private String text;

    @Getter @Setter
    private float score;

    /**
     * Compares on score first, then text next
     * @param o The object to compare to
     * @return The compare results
     */
    @Override
    public int compareTo(BeerCompletionResult o) {
        int results = Float.compare(this.getScore(), o != null ? o.getScore() : 0.0f);
        if (results == 0) {
            results = this.text.compareTo(o != null ? o.getText() : "");
        }
        return results;
    }
}
