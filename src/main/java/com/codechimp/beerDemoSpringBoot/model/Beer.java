package com.codechimp.beerDemoSpringBoot.model;

import lombok.*;

import javax.persistence.*;

/**
 * Entity that represents some Beer data
 */
@Entity
@NoArgsConstructor @AllArgsConstructor
@ToString @EqualsAndHashCode
public class Beer {
    public Beer(String name, String description, double abv, double ibu, BeerStyle style, Brewery brewery) {
        this.name = name;
        this.description = description;
        this.abv = abv;
        this.ibu = ibu;
        this.style = style;
        this.brewery = brewery;
    }

    @Id @GeneratedValue
    @Getter @Setter
    private long id;

    @Column
    @Getter @Setter
    private String name;

    @Column(length = 4096)
    @Getter @Setter
    private String description;

    @Column
    @Getter @Setter
    private double abv;

    @Column
    @Getter @Setter
    private double ibu;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "brewery_id")
    @Getter @Setter
    private Brewery brewery;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "style_id")
    @Getter @Setter
    private BeerStyle style;
}
