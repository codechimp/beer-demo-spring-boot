package com.codechimp.beerDemoSpringBoot.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity that respresents Beer Style data
 */
@Entity
@NoArgsConstructor @AllArgsConstructor
@ToString @EqualsAndHashCode(exclude = {"beers"})
public class BeerStyle {

    /**
     * Creates a {@link BeerStyle} with the given <code>name</code> and <code>description</code>
     * @param name The name
     * @param description The description
     */
    public BeerStyle(String name, String description) {
        this(0, name, description, null);
    }

    @Id
    @GeneratedValue
    @Getter @Setter
    private long id;

    @Column
    @Getter @Setter
    private String name;

    @Column
    @Getter @Setter
    private String description;

    @OneToMany(mappedBy = "style")
    @Setter
    private List<Beer> beers = new ArrayList<>();

    public List<Beer> getBeers() {
        if (beers == null) beers = new ArrayList<>();
        return beers;
    }

    /**
     * Appends a {@link Beer} using lazy init if needed
     * @param beer The <code>Beer</code>
     */
    public void appendBeer(Beer beer) {
        if (!getBeers().contains(beer)) this.getBeers().add(beer);
    }
}
