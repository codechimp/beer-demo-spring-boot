package com.codechimp.beerDemoSpringBoot.model;

/**
 * Interface used to mark Elasticsearch documents that are "scorable"
 */
public interface Scoreable {
    /**
     * Returns the document score.
     * @return The score
     */
    float getScore();

    /**
     * Sets the document score.
     * @param score The new score
     */
    void setScore(float score);
}
