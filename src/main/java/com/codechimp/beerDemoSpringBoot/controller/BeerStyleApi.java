package com.codechimp.beerDemoSpringBoot.controller;

import com.codechimp.beerDemoSpringBoot.model.BeerStyle;
import com.codechimp.beerDemoSpringBoot.service.BeerStyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for the {@link BeerStyle} API
 */
@RestController
@RequestMapping("/api/v1/beerstyle")
public class BeerStyleApi {
    @Autowired BeerStyleService styleSvc;

    /**
     * Retuns a single {@link BeerStyle} by it's Id
     * @param id The Id to find
     * @return The <code>BeerStyle</code>
     */
    @RequestMapping("/{id}")
    public BeerStyle getBeerStyle(@PathVariable("id") long id) {
        return styleSvc.findBeerStyleById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("BeerStyle id %d", id)));
    }

    /**
     * Returns a {@link Page} of {@link BeerStyle}s
     * @param pageable The {@link Pageable} containing the desired page, count, etc
     * @return The <code>Page</code> containing the paged results
     */
    @RequestMapping(method = RequestMethod.GET)
    public Page<BeerStyle> getBeerStyles(final Pageable pageable) {
        return styleSvc.getBeerStyles(pageable != null ? pageable : PageRequest.of(0, 10));
    }

    /**
     * Creates a new {@link BeerStyle}
     * @param style The <code>BeerStyle</code> to create
     * @return The resulting <code>BeerStyle</code> after it's created
     */
    @RequestMapping(method = RequestMethod.PUT)
    public BeerStyle createBeerStyle(@RequestBody BeerStyle style) {
        return styleSvc.saveBeerStyle(style);
    }

    /**
     * Saves a {@link BeerStyle}
     * @param style The <code>BeerStyle</code> to save
     * @return The resulting <code>BeerStyle</code> after it's saved
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public BeerStyle saveBeerStyle(@PathVariable("id") long id, @RequestBody BeerStyle style) {
        return styleSvc.saveBeerStyle(style);   // TODO: Should really throw a ResourceNotFoundException if not found here to make this a true "save"
    }

    /**
     * Removes a {@link BeerStyle}
     * @param id The Id of the <code>BeerStyle</code> to remove
     */
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public void removeBeerStyle(@PathVariable("id") long id) {
        styleSvc.removeBeerStyle(id);
    }
}
