package com.codechimp.beerDemoSpringBoot.controller;

import com.codechimp.beerDemoSpringBoot.model.Brewery;
import com.codechimp.beerDemoSpringBoot.service.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for the {@link Brewery} API
 */
@RestController
@RequestMapping("/api/v1/brewery")
public class BreweryApi {

    @Autowired private BreweryService brewerySvc;

    /**
     * Returns a single {@link Brewery} by id
     * @param id The <code>Brewery</code> id
     * @return The <code>Brewery</code>
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Brewery getBrewery(@PathVariable("id") long id) {
        return brewerySvc.findBreweryById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Brewery id %d", id)));
    }

    /**
     * Returns a {@link Page} of {@link Brewery}s
     * @param pageable The {@link Pageable} containing the page, count, etc
     * @return The resulting <code>Page</code> or <code>Brewery</code>s
     */
    @RequestMapping(method = RequestMethod.GET)
    public Page<Brewery> getBreweries(Pageable pageable) {
        return brewerySvc.getBreweries(pageable != null ? pageable : PageRequest.of(0, 10));
    }

    /**
     * Creates a new {@link Brewery}
     * @param brewery The <code>Brewery</code> to create
     * @return The resulting <code>Brewery</code> after creation
     */
    @RequestMapping(method = RequestMethod.PUT)
    public Brewery createBrewery(@RequestBody Brewery brewery) {
        return brewerySvc.saveBrewery(brewery);
    }

    /**
     * Saves a new {@link Brewery}
     * @param brewery The <code>Brewery</code> to create
     * @return The resulting <code>Brewery</code> after creation
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Brewery saveBrewery(@PathVariable("id") long id, @RequestBody Brewery brewery) {
        return brewerySvc.saveBrewery(brewery); // TODO: Should really throw a ResourceNotFoundException if not found here to make this a true "save"
    }

    /**
     * Removes a {@link Brewery}
     * @param id The <code>Brewery</code> id to remove
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeBrewery(@PathVariable("id") long id) {
        brewerySvc.removeBrewery(id);
    }
}
