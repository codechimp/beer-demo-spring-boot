package com.codechimp.beerDemoSpringBoot.controller;

import com.codechimp.beerDemoSpringBoot.model.Beer;
import com.codechimp.beerDemoSpringBoot.service.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for the {@link Beer} API
 */
@CrossOrigin(origins = {"http://localhost:8082", "http://localhost"}, maxAge = 3600)
@RestController
@RequestMapping("/api/v1/beer")
public class BeerApi {

    @Autowired
    private BeerService beerSvc;

    /**
     * Returns a single {@link Beer} by it's Id
     * @param id The <code>Beer</code> id
     * @return The <code>Beer</code>
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Beer getBeer(@PathVariable final long id) {
        return beerSvc.findBeerById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Beer ID %d ", id)));
    }

    /**
     * Returns a {@link Page} of {@link Beer} results
     * @param pageable The {@link Pageable} containing the page, count, etc
     * @return The resulting <code>Page</code> of <code>Beer</code> results
     */
    @RequestMapping(method = RequestMethod.GET)
    public Page<Beer> getBeers(final Pageable pageable) {
        return beerSvc.getBeers(pageable == null ? PageRequest.of(0, 10) : pageable);
    }

    /**
     * Creates a {@link Beer}
     * @param beer The <code>Beer</code> to create/save
     * @return The resulting <code>Beer</code> after it's saved
     */
    @RequestMapping(method = RequestMethod.PUT)
    public Beer createBeer(@RequestBody Beer beer) {
        return beerSvc.saveBeer(beer);
    }

    /**
     * Saves a {@link Beer}
     * @param beer The <code>Beer</code> to save
     * @return The resulting <code>Beer</code> after it's saved
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Beer saveBeer(@PathVariable("id") long id, @RequestBody Beer beer) {
        return beerSvc.saveBeer(beer);  // TODO: Should really throw a ResourceNotFoundException if not found here to make this a true "save"
    }

    /**
     * Removes a {@link Beer}
     * @param id The Id of the <code>Beer</code> to remove
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeBeer(@PathVariable final long id) {
        beerSvc.removeBeer(id);
    }
}
