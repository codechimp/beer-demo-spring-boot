package com.codechimp.beerDemoSpringBoot.repository;

import com.codechimp.beerDemoSpringBoot.model.Beer;
import com.codechimp.beerDemoSpringBoot.model.Brewery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository for {@link Beer} entities using JPA
 */
@Repository
public interface BeerJpaRepository extends JpaRepository<Beer, Long> {

    /**
     * Retrieves a {@link Beer} by name
     * @param name The <code>Beer</code> name
     * @return The {@link Optional} containing the <code>Beer</code> or null
     */
    Optional<Beer> findByName(String name);

    /**
     * Finds a {@link Beer} by it's name and it's source id
     * @param name The name
     * @param brewery The {@link Brewery} that produces this beer
     * @return The {@link Optional} containing the <code>Beer</code> or null
     */
    Optional<Beer> findByNameAndBrewery(String name, Brewery brewery);
}
