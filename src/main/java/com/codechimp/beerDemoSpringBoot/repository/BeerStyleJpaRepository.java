package com.codechimp.beerDemoSpringBoot.repository;

import com.codechimp.beerDemoSpringBoot.model.BeerStyle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository for {@link BeerStyle} entities using JPA
 */
@Repository
public interface BeerStyleJpaRepository extends JpaRepository<BeerStyle, Long> {

    /**
     * Returns a {@link BeerStyle} by it's name
     * @param name The name
     * @return The resulting BeerStyle
     */
    Optional<BeerStyle> findByName(String name);
}
