package com.codechimp.beerDemoSpringBoot.repository;

import com.codechimp.beerDemoSpringBoot.model.Brewery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository for {@link Brewery} entities using JPA
 */
@Repository
public interface BreweryJpaRepository extends JpaRepository<Brewery, Long> {

    /**
     * Finds a {@link Brewery} by name
     * @param name The <code>Brewery</code> name
     * @return The <code>Brewery</code>
     */
    Optional<Brewery> findByName(String name);
}
