package com.codechimp.beerDemoSpringBoot.repository;

import com.codechimp.beerDemoSpringBoot.model.BeerDocument;
import org.elasticsearch.index.query.DisMaxQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.Arrays;

/**
 * Repository for {@link BeerDocument} documents using Elasticsearch
 */
@Repository
public interface BeerDocumentESRepository extends ElasticsearchRepository<BeerDocument, Long> {
    String ES_STRING_WILDCARD = "*";

    Page<BeerDocument> findByName(String name, Pageable pageable);

    /**
     * Finds by name containing.
     *
     * <p>The search string is split by whitespaces, then the search is done as follows:
     * <ol>
     *     <li>Exact match as passed in (with spaces)</li>
     *     <li>String match using tokenized values, first as-is then with wildcards around</li>
     *     <li>Common terms</li>
     * </ol>
     * </p>
     *
     * @param name The name search string
     * @param pageable The pageable to determine which page to return
     * @return The {@link Page} of results
     */
    default Page<BeerDocument> findByNameContaining(String name, Pageable pageable) {
        DisMaxQueryBuilder qb = QueryBuilders.disMaxQuery();
        qb.add(
                QueryBuilders.matchQuery("name", name).boost(2.0f)
        );
        Arrays.stream(name.split("\\s")).forEach(s -> {
            qb.add(
                    QueryBuilders.boolQuery()
                    .should(
                            QueryBuilders.queryStringQuery(s)
                                .lenient(true)
                                .field("name")
                                .boost(1.5f)
                    )
                    .should(
                            QueryBuilders.queryStringQuery(ES_STRING_WILDCARD + s + ES_STRING_WILDCARD)
                                    .lenient(true)
                                    .field("name")
                                    .boost(1.2f)
                    )
            );
        });
        qb.add(
                QueryBuilders.commonTermsQuery("name", name)
        );

        return this.search(qb, pageable);
    }

    Page<BeerDocument> findByStyleName(String styleName, Pageable pageable);

    Page<BeerDocument> findByBreweryName(String breweryName, Pageable pageable);

    Page<BeerDocument> findByAbvBetween(double abvLow, double abvHigh, Pageable pageable);
}
