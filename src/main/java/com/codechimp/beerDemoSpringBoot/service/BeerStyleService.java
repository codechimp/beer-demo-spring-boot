package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.model.BeerStyle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service interface for {@link BeerStyle}s
 */
public interface BeerStyleService {
    /**
     * Saves/updates a {@link BeerStyle} entity
     * @param beerStyle The <code>BeerStyle</code> to save
     * @return The resulting <code>BeerStyle</code> after it's saved
     */
    BeerStyle saveBeerStyle(BeerStyle beerStyle);

    /**
     * Returns an {@link Optional} single {@link BeerStyle} by it's Id
     * @param id The <code>BeerStyle</code>  Id
     * @return An <code>Optional</code> containing the <code>BeerStyle</code> or null
     */
    Optional<BeerStyle> findBeerStyleById(long id);

    /**
     * Returns a {@link BeerStyle} by it's name
     * @param name The <code>BeerStyle</code> name
     * @return The resulting <code>BeerStyle</code> or null
     */
    Optional<BeerStyle> findBeerStyleByName(String name);

    /**
     * Returns a {@link Page} of {@link BeerStyle}s
     * @param pageable The {@link Pageable} containing the desired page, count, etc.
     * @return A <code>Page</code> of resulting <code>BeerStyle</code>s
     */
    Page<BeerStyle> getBeerStyles(Pageable pageable);

    /**
     * Returns a {@link List} of all {@link BeerStyle}s
     * @return A <code>List</code> of all <code>BeerStyle</code>s
     */
    List<BeerStyle> getBeerStyles();

    /**
     * Removes a {@link BeerStyle}
     * @param id The <code>BeerStyle</code> Id to remove
     */
    void removeBeerStyle(long id);
}
