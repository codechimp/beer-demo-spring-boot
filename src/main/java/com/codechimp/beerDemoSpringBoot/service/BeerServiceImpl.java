package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.model.*;
import com.codechimp.beerDemoSpringBoot.repository.BeerDocumentESRepository;
import com.codechimp.beerDemoSpringBoot.repository.BeerJpaRepository;
import com.codechimp.beerDemoSpringBoot.repository.BeerStyleJpaRepository;
import com.codechimp.beerDemoSpringBoot.repository.BreweryJpaRepository;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @see BeerService
 */
@Service
public class BeerServiceImpl implements BeerService {
    @Autowired private BeerJpaRepository beerJpaRepo;
    @Autowired private BeerStyleJpaRepository beerStyleJpaRepo;
    @Autowired private BreweryJpaRepository breweryJpaRepo;
    @Autowired private BeerDocumentESRepository beerEsRepo;

    @Autowired private ElasticsearchTemplate esTemplate;

    /**
     * @see BeerService#saveBrewery(Brewery)
     */
    public Brewery saveBrewery(Brewery brewery) {
        return breweryJpaRepo.save(brewery);
    }

    /**
     * @see BeerService#saveBeer(Beer)
     */
    public Beer saveBeer(Beer beer) {
        return saveBeer(beer, null, null);
    }

    /**
     * @see BeerService#saveBeer(Beer, Long, Long)
     */
    public Beer saveBeer(Beer beer, Long beerStyleId, Long breweryId) {
        // Set the related bits
        Brewery brewery = breweryId != null ? breweryJpaRepo.findById(breweryId).orElse(null) : null;
        BeerStyle style = beerStyleId != null ? beerStyleJpaRepo.findById(beerStyleId).orElse(null) : null;
        beer.setBrewery(brewery);
        beer.setStyle(style);

        // Now save the info off to Elasticsearch
        Beer resultBeer = beerJpaRepo.save(beer);
        BeerDocument beerDoc = beerEsRepo.findById(resultBeer.getId()).orElse(new BeerDocument());
        beerDoc.setId(resultBeer.getId());
        beerDoc.setName(resultBeer.getName());
        beerDoc.setName_completion(new Completion(new String[] {resultBeer.getName()}));
        beerDoc.setDescription(resultBeer.getDescription());
        beerDoc.setDescription_completion(new Completion(new String[] {resultBeer.getDescription() != null ? resultBeer.getDescription() : ""}));
        beerDoc.setAbv(resultBeer.getAbv());
        beerDoc.setIbu(resultBeer.getIbu());
        if (style != null) {
            beerDoc.setStyleId(style.getId());
            beerDoc.setStyleName(style.getName());
            beerDoc.setStyleDescription(style.getDescription());
        }
        if (brewery != null) {
            beerDoc.setBreweryId(brewery.getId());
            beerDoc.setBreweryName(brewery.getName());
            beerDoc.setBreweryDescription(brewery.getDescription());
        }
        beerEsRepo.save(beerDoc);

        return resultBeer;
    }

    /**
     * @see BeerService#findBeerById(long)
     */
    @Override
    public Optional<Beer> findBeerById(long id) {
        return beerJpaRepo.findById(id);
    }

    /**
     * @see BeerService#findBeerByName(String)
     */
    @Override
    public Optional<Beer> findBeerByName(String name) {
        return beerJpaRepo.findByName(name);
    }

    /**
     * @see BeerService#findBeerByNameAndBrewery(String, Brewery)
     */
    public Optional<Beer> findBeerByNameAndBrewery(String name, Brewery brewery) {
        return beerJpaRepo.findByNameAndBrewery(name, brewery);
    }

    /**
     *  @see BeerService#getBeers(Pageable)
     */
    public Page<Beer> getBeers(final Pageable pageable) {
        return beerJpaRepo.findAll(pageable);
    }

    /**
     *  @see BeerService#getBeers()
     */
    public List<Beer> getBeers() {
        return beerJpaRepo.findAll();
    }

    /**
     * @see BeerService#removeBeer(long)
     */
    public void removeBeer(long id) {
        beerJpaRepo.deleteById(id);
        beerEsRepo.deleteById(id);
    }

    /**
     * @see BeerService#searchBeersByName(String)
     */
    @Override
    public Page<BeerDocument> searchBeersByName(String name) {
        return searchBeersByName(name, PageRequest.of(0, 10));
    }

    /**
     * @see BeerService#searchBeersByName(String, PageRequest)
     */
    @Override
    public Page<BeerDocument> searchBeersByName(String name, PageRequest pageRequest) {
        return beerEsRepo.findByNameContaining(name, pageRequest);
    }

    /**
     * @see BeerService#typeAheadSearchBeers(String)
     */
    @Override
    public List<BeerCompletionResult> typeAheadSearchBeers(String search) {
        // We have to build the SearchBuilder manually since it appears Spring Data's Elasticsearch libraries do not offer an easier path for suggestions
        Set<BeerCompletionResult> results = new TreeSet<>();
        SearchResponse response = esTemplate.suggest(
                new SuggestBuilder()
                        .addSuggestion("name_suggestions",SuggestBuilders.completionSuggestion("name_completion"))
                        .addSuggestion("description_suggestions", SuggestBuilders.completionSuggestion("description_completion"))
                        .setGlobalText(search)
                , BeerDocument.class);

        response.getSuggest().getSuggestion("name_suggestions")
                .getEntries()
                .stream()
                .forEach(
                        entry -> ((CompletionSuggestion.Entry) entry)
                                .getOptions()
                                .forEach( option -> {
                                    BeerCompletionResult compResults = new BeerCompletionResult();
                                    compResults.setId(Long.parseLong(option.getHit().getId()));
                                    compResults.setText(option.getText().string());
                                    compResults.setScore(option.getScore());
                                    results.add(compResults);
                                })
                );

        return new ArrayList<>(results);
    }
}
