package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.model.BeerStyle;
import com.codechimp.beerDemoSpringBoot.repository.BeerStyleJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of the {@link BeerStyleService}
 */
// TODO Need to fix/cleanup ES when we alter a BeerStyle since the documents in ES reference the ID, Name and Description
@Service
public class BeerStyleServiceImpl implements BeerStyleService {

    @Autowired private BeerStyleJpaRepository styleJpaRepo;

    /**
     * @see BeerStyleService#saveBeerStyle(BeerStyle)
     */
    @Override
    public BeerStyle saveBeerStyle(BeerStyle beerStyle) {
        return styleJpaRepo.save(beerStyle);
    }

    /**
     * @see BeerStyleService#findBeerStyleById(long)
     */
    @Override
    public Optional<BeerStyle> findBeerStyleById(long id) {
        return styleJpaRepo.findById(id);
    }

    /**
     *  @see BeerStyleService#findBeerStyleByName(String)
     */
    public Optional<BeerStyle> findBeerStyleByName(String name) {
        return styleJpaRepo.findByName(name);
    }

    /**
     * @see BeerStyleService#getBeerStyles(Pageable)
     */
    @Override
    public Page<BeerStyle> getBeerStyles(Pageable pageable) {
        return styleJpaRepo.findAll(pageable);
    }

    /**
     * @see BeerStyleService#getBeerStyles()
     */
    @Override
    public List<BeerStyle> getBeerStyles() {
        return styleJpaRepo.findAll();
    }

    /**
     * @see BeerStyleService#removeBeerStyle(long)
     */
    @Override
    public void removeBeerStyle(long id) {
        styleJpaRepo.deleteById(id);
    }
}
