package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.model.Brewery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service interface for {@link Brewery}s
 */
public interface BreweryService {
    /**
     * Saves a {@link Brewery}
     * @param brewery The <code>Brewery</code> to save
     * @return The resulting <code>Brewery</code> after save
     */
    Brewery saveBrewery(Brewery brewery);

    /**
     * Finds a {@link Brewery} by it's Id
     * @param id The Id
     * @return An {@link Optional} containing the <code>Brewery</code> or null
     */
    Optional<Brewery> findBreweryById(long id);

    /**
     * Retrieves a {@link Brewery} by name
     * @param name the <code>Brewery</code> name
     * @return The <code>Brewery</code> or null
     */
    Optional<Brewery> findBreweryByName(String name);

    /**
     * Gets a {@link Page} or {@link Brewery}s
     * @param pageable The {@link Pageable} containing the desired page, count, etc.
     * @return A {@link Page} of {@link Brewery}s
     */
    Page<Brewery> getBreweries(Pageable pageable);

    /**
     * Gets all {@link Brewery}s
     * @return A {@link List} of all <code>Brewery</code>s
     */
    List<Brewery> getBreweries();

    /**
     * Removes a {@link Brewery}
     * @param id The <code>Brewery</code> id
     */
    void removeBrewery(long id);
}
