package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.model.Brewery;
import com.codechimp.beerDemoSpringBoot.repository.BreweryJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of the {@link BreweryService}
 */
@Service
public class BreweryServiceImpl implements BreweryService {

    @Autowired private BreweryJpaRepository breweryRepo;

    /**
     * @see BreweryService#saveBrewery(Brewery)
     */
    @Override
    public Brewery saveBrewery(Brewery brewery) {
        return breweryRepo.save(brewery);
    }

    /**
     * @see BreweryService#findBreweryById(long)
     */
    @Override
    public Optional<Brewery> findBreweryById(long id) {
        return breweryRepo.findById(id);
    }

    /**
     * @see BreweryService#findBreweryByName(String)
     */
    @Override
    public Optional<Brewery> findBreweryByName(String name) {
        return breweryRepo.findByName(name);
    }

    /**
     * @see BreweryService#getBreweries(Pageable)
     */
    @Override
    public Page<Brewery> getBreweries(Pageable pageable) {
        return breweryRepo.findAll(pageable);
    }

    /**
     * @see BreweryService#getBreweries()
     */
    @Override
    public List<Brewery> getBreweries() {
        return breweryRepo.findAll();
    }

    /**
     * @see BreweryService#removeBrewery(long)
     */
    @Override
    public void removeBrewery(long id) {
        breweryRepo.deleteById(id);
    }
}
