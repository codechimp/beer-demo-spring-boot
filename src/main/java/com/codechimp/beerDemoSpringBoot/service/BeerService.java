package com.codechimp.beerDemoSpringBoot.service;

import com.codechimp.beerDemoSpringBoot.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service interface for the Beer app
 */
public interface BeerService {
    /**
     * Saves/updates a {@link Brewery} entity
     * @param brewery The <code>Brewery</code> to save
     * @return The resulting <code>Brewery</code> after it's saved
     */
    Brewery saveBrewery(Brewery brewery);

    /**
     * Saves/updates a {@link Beer} entity
     * @param beer The <code>Beer</code> to save
     * @return The resulting <code>Beer</code> after it's saved
     *
     * @see BeerService#saveBeer(Beer, Long, Long)
     */
    Beer saveBeer(Beer beer);

    /**
     * Saves/updates a {@link Beer} entity and links it with the given {@link BeerStyle} and {@link Brewery}
     *
     * <p>In addition to saving the information to the database, this method will also store the relavent bits into
     * Elasticsearch for searchability</p>
     *
     * @param beer The <code>Beer</code> to save
     * @param beerStyleId The ID of the {@link BeerStyle} for this beer
     * @param breweryId The ID of the {@link Brewery} for this beer
     * @return The resulting <code>Beer</code> after it's saved
     */
    Beer saveBeer(Beer beer, Long beerStyleId, Long breweryId);

    /**
     * Returns a single {@link Beer} by it's ID
     * @param id The ID for the <code>Beer</code>
     * @return An {@link Optional} containing the <code>Beer</code> or <code>null</code>
     */
    Optional<Beer> findBeerById(long id);

    /**
     * Returns a single {@link Beer} by it's name
     * @param name The name for the <code>Beer</code>
     * @return An {@link Optional} containing the <code>Beer</code> or <code>null</code>
     */
    Optional<Beer> findBeerByName(String name);

    /**
     * Returns a single {@link Beer} found by it's name and the {@link Brewery} that produces it
     * @param name The name of the <code>Beer</code>
     * @param brewery The <code>Brewery</code> that makes the <code>Beer</code>
     * @return The <code>Beer</code>
     */
    Optional<Beer> findBeerByNameAndBrewery(String name, Brewery brewery);

    /**
     * Returns all {@link Beer}s, paged
     * @param pageable The {@link Pageable} containing info on which page and the count to return
     * @return The {@link Page} containing the requested results
     */
    Page<Beer> getBeers(final Pageable pageable);

    /**
     * Returns ALL {@link Beer}s
     * @return A {@link List} of all <code>Beer</code>s
     */
    List<Beer> getBeers();

    /**
     * Removes a {@link Beer} by it's ID
     * @param id The ID to remove
     */
    void removeBeer(long id);

    /**
     * Returns a {@link Page} of {@link BeerDocument} results where the {@link Beer}'s <code>name</code> contains the
     * given string, defaulting to the first page of 10 results
     * @param name The <code>Beer</code> name to search
     * @return A <code>Page</code> of <code>BeerDocument</code>s matching the given name
     */
    Page<BeerDocument> searchBeersByName(String name);

    /**
     * Returns a {@link Page} of {@link BeerDocument} results where the {@link Beer}'s <code>name</code> contains the given string
     * @param name The <code>Beer</code> name to search
     * @param pageRequest The {@link PageRequest} containing the desired results page
     * @return A <code>Page</code> of <code>BeerDocument</code>s matching the given name
     */
    Page<BeerDocument> searchBeersByName(String name, PageRequest pageRequest);

    /**
     * Performs a type-ahead search using the given search string
     * @param search The search string to search on
     * @return The results
     */
    List<BeerCompletionResult> typeAheadSearchBeers(String search);
}
