package com.codechimp.beerDemoSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Spring Boot application
 */
@SpringBootApplication
@EnableScheduling
@EnableTransactionManagement
public class BeerDemoSpringBootApplication {

	/**
	 * Starts the Spring Boot app
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(BeerDemoSpringBootApplication.class, args);
	}
}
