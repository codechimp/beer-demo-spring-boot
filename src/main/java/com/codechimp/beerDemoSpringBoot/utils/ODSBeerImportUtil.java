package com.codechimp.beerDemoSpringBoot.utils;

import com.codechimp.beerDemoSpringBoot.model.Beer;
import com.codechimp.beerDemoSpringBoot.model.BeerStyle;
import com.codechimp.beerDemoSpringBoot.model.Brewery;
import com.codechimp.beerDemoSpringBoot.service.BeerService;
import com.codechimp.beerDemoSpringBoot.service.BeerStyleService;
import com.codechimp.beerDemoSpringBoot.service.BreweryService;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;

@Component
public class ODSBeerImportUtil {
    @Value("${opendatasoft.api.beer.url}") private String odsBeerUrl;

    @Autowired
    private BeerStyleService styleSvc;
    @Autowired
    private BreweryService brewerySvc;
    @Autowired
    private BeerService beerSvc;

    private static final Logger log = LoggerFactory.getLogger(ODSBeerImportUtil.class);

    @Transactional
    @Scheduled(fixedRate = (1L * 1000L * 60L *60L))
    public void loadOpenDataSoftBeerData() {
        log.info("Loading beer data from Open Data Soft");
        long recordsCnt = 0;
        long toProcessCount = 0;
        long returnedRows = 0;
        try {
            do {
                String response = getJSonRecords(20, recordsCnt);
                ObjectMapper mapper = new ObjectMapper();
                JsonFactory factory = mapper.getFactory();
                JsonParser parser = factory.createParser(response);
                JsonNode rootNode = mapper.readTree(parser);
                toProcessCount = rootNode.at("/nhits").asLong(0);
                JsonNode records = rootNode.get("records");
                returnedRows = records.size();
                recordsCnt += StreamSupport.stream(Spliterators.spliteratorUnknownSize(records.elements(), Spliterator.ORDERED), false)
                        .peek(node -> {
                            String styleName = StringUtils.trimToNull(node.at("/fields/style_name").asText());
                            BeerStyle style = null;
                            if (styleName != null) {
                                try {
                                    style = styleSvc.findBeerStyleByName(styleName)
                                            .orElseGet(() -> styleSvc.saveBeerStyle(new BeerStyle(styleName, null)));
                                } catch(IncorrectResultSizeDataAccessException e) {
                                    style = null;
                                }
                            }

                            String breweryName = StringUtils.trimToNull(node.at("/fields/name_breweries").asText());
                            Brewery brewery = null;
                            if (breweryName != null) {
                                brewery = brewerySvc.findBreweryByName(styleName)
                                        .orElseGet(() -> brewerySvc.saveBrewery(new Brewery(breweryName, null)));
                            }

                            String beerName = StringUtils.trimToNull(node.at("/fields/name").asText());
                            Beer beer = null;
                            if (beerName != null) {
                                Optional<Beer> optBeer = beerSvc.findBeerByNameAndBrewery(styleName, brewery);
                                if (optBeer.isPresent()) {
                                    beer = optBeer.get();
                                } else {
                                    beer = beerSvc.saveBeer(new Beer(
                                            node.at("/fields/name").asText(),
                                            node.at("/fields/descript").asText("NONE"),
                                            node.at("/fields/abv").asDouble(),
                                            node.at("/fields/ibu").asDouble(),
                                            style,
                                            brewery));
                                }
                            }
                        })
                        .count();
                log.info(String.format("Processed %d of %d records", recordsCnt, toProcessCount));
            } while(recordsCnt < toProcessCount || returnedRows == 0);
        } catch (IOException e) {
            log.error("Could not load Beer data from Open Data Soft");
        }
        log.info(String.format("Finished loading %d records from Open Data Soft", recordsCnt));
    }

    private String getJSonRecords(Integer rows, Long start) throws IOException {
        StringBuilder rawUrl = new StringBuilder(odsBeerUrl);
        if (rows != null) rawUrl.append(String.format("&rows=%d", rows));
        if (start != null) rawUrl.append(String.format("&start=%d", start));
        URL url = new URL(rawUrl.toString());
        String response = readAll(new InputStreamReader(url.openConnection().getInputStream()));
        return response;
    }

    private String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
