package com.codechimp.beerDemoSpringBoot.config;

import com.codechimp.beerDemoSpringBoot.model.Scoreable;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.InternalSettingsPreparer;
import org.elasticsearch.node.Node;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.Netty3Plugin;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.DefaultResultMapper;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.convert.MappingElasticsearchConverter;
import org.springframework.data.elasticsearch.core.mapping.ElasticsearchPersistentEntity;
import org.springframework.data.elasticsearch.core.mapping.ElasticsearchPersistentProperty;
import org.springframework.data.elasticsearch.core.mapping.SimpleElasticsearchMappingContext;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

/**
 * Configuration class for Elasticsearch
 */
@Configuration
@EnableElasticsearchRepositories(basePackages = "com.codechimp.beerDemoSpringBoot.repository")
public class ElasticSearchConfiguration {
    @Value("${elasticsearch.embedded:false}") private boolean embedded;
    @Value("${elasticsearch.host:}") private String esHost;
    @Value("${elasticsearch.port:9300}") private int esPort;
    @Value("${elasticsearch.clustername:elasticsearch}") private String esClusterName;

    private static final String HTTP_PORT = "9205";
    //private static final String HTTP_TRANSPORT_PORT = "9305";
    private static final String ES_WORKING_DIR = "target/es";

    private static Node node;

    private static final Logger log = LoggerFactory.getLogger(ElasticSearchConfiguration.class);

    /**
     * Returns the Elasticsearch {@link Client} bean configured using the app.properties
     * @return The <code>Client</code> bean
     * @throws Exception Any exception that might occur
     */
    @Bean
    public Client client() throws Exception {
        if (node != null) {
            stopElasticsearch();
            node = null;
        }

        if (embedded) {
            log.warn("Starting Elasticsearch as 'embedded'");
            return startElasticsearch();
        } else {
            Settings clientSettings = Settings.builder()
                    .put("cluster.name", esClusterName)
                    .build();
            return new PreBuiltTransportClient(clientSettings)
                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(esHost), esPort));
        }
    }

    /**
     * Returns the {@link ElasticsearchTemplate} bean using the {@link Client}
     * @return The <code>ElasticsearchTemplate</code> bean
     * @throws Exception Any exception that might occur
     */
    @Bean
    public ElasticsearchTemplate elasticsearchTemplate() throws Exception {
        MappingElasticsearchConverter converter = new MappingElasticsearchConverter(new SimpleElasticsearchMappingContext());
        ScoreResultsMapper mapper = new ScoreResultsMapper(converter.getMappingContext());
        return new ElasticsearchTemplate(client(), converter, mapper);
    }

    public Client startElasticsearch() throws Exception {
        log.info("Starting embedded Elasticsearch");
        removeOldDataDir(ES_WORKING_DIR);

        Settings settings = Settings.builder()
                .put("path.home", ES_WORKING_DIR)
                .put("path.conf", ES_WORKING_DIR)
                .put("path.data", ES_WORKING_DIR)
                .put("path.logs", ES_WORKING_DIR)
                .put("cluster.name", esClusterName)
                .put("http.port", HTTP_PORT)
                .put("http.type","netty3")
                .put("transport.tcp.port", esPort)
                .put("transport.type","local")
                .build();

        node = new PluginConfigurableNode(settings, Collections.singletonList(Netty3Plugin.class));
        node.start();
        return node.client();
    }

    public void stopElasticsearch() throws Exception {
        log.info("Stopping embedded Elasticsearch");
        if (node != null) node.close();
        node = null;
    }

    private void removeOldDataDir(String datadir) throws Exception {
        File dataDir = new File(datadir);
        if (dataDir.exists()) {
            FileSystemUtils.deleteRecursively(dataDir);
        }
    }

    /**
     * Extension of the {@link Node} class that allows setting plugins
     */
    private class PluginConfigurableNode extends Node {
        PluginConfigurableNode(Settings settings, Collection<Class<? extends Plugin>> classpathPlugins) {
            super(InternalSettingsPreparer.prepareEnvironment(settings, null), classpathPlugins);
        }
    }

    /**
     * {@link org.springframework.data.elasticsearch.core.ResultsMapper} that has score support
     */
    public class ScoreResultsMapper extends DefaultResultMapper {

        ScoreResultsMapper(MappingContext<? extends ElasticsearchPersistentEntity<?>, ElasticsearchPersistentProperty> mappingContext) {
            super(mappingContext);
        }

        @Override
        public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
            AggregatedPage<T> resultPage = super.mapResults(response, clazz, pageable);
            Iterator<T> it = resultPage.getContent().iterator();
            for (SearchHit hit : response.getHits()) {
                if (hit != null) {
                    T next = it.next();
                    if (next instanceof  Scoreable) {
                        ((Scoreable) next).setScore(hit.getScore());
                    }
                }
            }
            return resultPage;
        }
    }
}
